var 
express               = require('express'),
http                  = require('http'),
fs                    = require('fs'),
applicationProperties = require('./configs'),
bodyParser            = require('body-parser'),
cookieParser          = require('cookie-parser'),
session               = require('express-session'),
app                   = express();



if (!fs.existsSync(applicationProperties.logDirectory)) {
  fs.mkdirSync(applicationProperties.logDirectory);
}

app.set('port', process.env.PORT || 8082 );


app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());
app.use(session({
  key: 'user_session',
  secret: 'dcdj@2019!',
  resave: false,
  saveUninitialized: false,
  cookie: {
    expires: 600000
  }
}));

app.use((req, res, next) => {
  if (req.cookies.user_session && !req.session.user) {
      res.clearCookie('activity_app');
  }
  next();
});


// middleware function to check for logged-in users
var sessionChecker = (req, res, next) => {
  if (req.session.user && req.cookies.user_session) {
      res.render('pages/dashboard');
  } else {
      next();
  }    
};

// Configurer le moteur de page EJS
app.set('view engine', 'ejs');

// Exemple de page INDEX 
app.get('/', function(req, res) {
    res.render('pages/bord');
});

// Exemple de page ABOUT
app.get('/apropos', function(req, res) {
    res.render('pages/about');
});

// page ABOUTRSB
/*
app.get('/apropos', function(req, res) {
    res.render('pages/abouts');
});
*/

// route for user's dashboard
app.get('/dashboard', (req, res) => {
  if (req.session.user && req.cookies.user_session) {
    res.render('pages/dashboard');
  } else {
      res.redirect('/login');
  }
});


app.route('/patients')
    .get(sessionChecker, (req, res) => {

      applicationProperties.db.query('SELECT * FROM patient p ORDER BY p._nom ASC', [])
      .then(( queryCallback) =>{
        console.log( queryCallback );
        res.render('pages/patients/list', { patients : queryCallback} );

      }).catch((error)=>{

          console.warn(error);
          applicationProperties.logger.warn('WARN '+ error);
      })

    });


app.route('/Activites')
    .get(sessionChecker, (req, res) => {

      applicationProperties.db.query('SELECT * FROM Activites a ORDER BY a._nom ASC', [])
      .then(( queryCallback) =>{
        console.log( queryCallback );
        res.render('pages/Activites/list', { Activites : queryCallback} );

      }).catch((error)=>{

          console.warn(error);
          applicationProperties.logger.warn('WARN '+ error);
      })

    });


app.route('/Services')
    .get(sessionChecker, (req, res) => {

      applicationProperties.db.query('SELECT * FROM Services s ORDER BY s._nom ASC', [])
      .then(( queryCallback) =>{
        console.log( queryCallback );
        res.render('pages/Services/list', { Services : queryCallback} );

      }).catch((error)=>{

          console.warn(error);
          applicationProperties.logger.warn('WARN '+ error);
      })

    });


app.route('/Examen')
        .get(sessionChecker, (req, res) => {

          applicationProperties.db.query('SELECT * FROM Examen e ORDER BY e._nom ASC', [])
          .then(( queryCallback) =>{
            console.log( queryCallback );
            res.render('pages/Examen/list', { Examens : queryCallback} );

          }).catch((error)=>{

              console.warn(error);
              applicationProperties.logger.warn('WARN '+ error);
          })

        });


app.route('/Maladies')
        .get(sessionChecker, (req, res) => {

          applicationProperties.db.query('SELECT * FROM Maladies m ORDER BY m._nom ASC', [])
          .then(( queryCallback) =>{
            console.log( queryCallback );
            res.render('pages/Maladies/list', { Maladies : queryCallback} );

          }).catch((error)=>{

              console.warn(error);
              applicationProperties.logger.warn('WARN '+ error);
          })

        });


app.route('/Correspondants')
        .get(sessionChecker, (req, res) => {

          applicationProperties.db.query('SELECT * FROM Maladies c ORDER BY c._nom ASC', [])
          .then(( queryCallback) =>{
            console.log( queryCallback );
            res.render('pages/Correspondants/list', { Correspondants : queryCallback} );

          }).catch((error)=>{

              console.warn(error);
              applicationProperties.logger.warn('WARN '+ error);
          })

        });


app.route('/Domaines')
        .get(sessionChecker, (req, res) => {

          applicationProperties.db.query('SELECT * FROM Domaines d ORDER BY d._nom ASC', [])
          .then(( queryCallback) =>{
            console.log( queryCallback );
            res.render('pages/Domaines/list', { Domaines : queryCallback} );

          }).catch((error)=>{

              console.warn(error);
              applicationProperties.logger.warn('WARN '+ error);
          })

        });


app.route('/Personnels')
        .get(sessionChecker, (req, res) => {

          applicationProperties.db.query('SELECT * FROM Personnels p ORDER BY p._nom ASC', [])
          .then(( queryCallback) =>{
            console.log( queryCallback );
            res.render('pages/Personnels/list', { Personnels : queryCallback} );

          }).catch((error)=>{

              console.warn(error);
              applicationProperties.logger.warn('WARN '+ error);
          })

        });


app.route('/Suivis')
        .get(sessionChecker, (req, res) => {

          applicationProperties.db.query('SELECT * FROM Suivis s ORDER BY s._nom ASC', [])
          .then(( queryCallback) =>{
            console.log( queryCallback );
            res.render('pages/Suivis/list', { Suivis : queryCallback} );

          }).catch((error)=>{

              console.warn(error);
              applicationProperties.logger.warn('WARN '+ error);
          })

        });


app.route('/Structures')
        .get(sessionChecker, (req, res) => {

          applicationProperties.db.query('SELECT * FROM Structures s ORDER BY s._nom ASC', [])
          .then(( queryCallback) =>{
            console.log( queryCallback );
            res.render('pages/Structures/list', { Structures : queryCallback} );

          }).catch((error)=>{

              console.warn(error);
              applicationProperties.logger.warn('WARN '+ error);
          })

        });


app.route('/Utilisateurs')
        .get(sessionChecker, (req, res) => {

          applicationProperties.db.query('SELECT * FROM Utilisateurs u ORDER BY u._nom ASC', [])
          .then(( queryCallback) =>{
            console.log( queryCallback );
            res.render('pages/Utilisateurs/list', { Utilisateurs : queryCallback} );

          }).catch((error)=>{

              console.warn(error);
              applicationProperties.logger.warn('WARN '+ error);
          })

        });


app.route('/Populations_cible')
        .get(sessionChecker, (req, res) => {

          applicationProperties.db.query('SELECT * FROM Populations_cible p ORDER BY p._nom ASC', [])
          .then(( queryCallback) =>{
            console.log( queryCallback );
            res.render('pages/Populations_mcible/list', { Populations_cible : queryCallback} );

          }).catch((error)=>{

              console.warn(error);
              applicationProperties.logger.warn('WARN '+ error);
          })

        });


app.route('/Projets')
        .get(sessionChecker, (req, res) => {

          applicationProperties.db.query('SELECT * FROM Projets p ORDER BY p._nom ASC', [])
          .then(( queryCallback) =>{
            console.log( queryCallback );
            res.render('pages/Projets/list', { Projets : queryCallback} );

          }).catch((error)=>{

              console.warn(error);
              applicationProperties.logger.warn('WARN '+ error);
          })

        });


app.route('/Traitements')
        .get(sessionChecker, (req, res) => {

          applicationProperties.db.query('SELECT * FROM Traitements t ORDER BY t._nom ASC', [])
          .then(( queryCallback) =>{
            console.log( queryCallback );
            res.render('pages/Traitements/list', { Traitements : queryCallback} );

          }).catch((error)=>{

              console.warn(error);
              applicationProperties.logger.warn('WARN '+ error);
          })

        });

// Exemple de page LOGIN
app.route('/login')
    .get(sessionChecker, (req, res) => {
      res.render('pages/login');
    });

app
  .route('/authenticate')   
  .post((req, res) => {

    console.info(req.body);
        const username = req.body.username,
              password = req.body.password;

            applicationProperties.db.query('SELECT u."Nom_User" FROM "Utilisateurs" u WHERE u."Nom_User" = $1 AND u."MdPass_User" = $3', [username, password])
            .then(( queryCallback) =>{

            // console.log(queryCallback);
            if (queryCallback.length === 0 ) {
                res.render('pages/login',{ errorMessage:" utilisateur inconnu" });
            } else {
                console.log( queryCallback);
                req.session.user = queryCallback[0]._username;
                res.redirect('/dashboard');
            }

            }).catch((error)=>{

                console.warn(error);
                applicationProperties.logger.warn('WARN '+ error);
            })



        // User.findOne({ where: { username: username } }).then(function (user) {
        //     if (!user) {
        //         res.redirect('/login');
        //     } else if (!user.validPassword(password)) {
        //         res.redirect('/login');
        //     } else {
        //         req.session.user = user.dataValues;
        //         res.redirect('/dashboard');
        //     }
        // });

    });

// route for handling 404 requests(unavailable routes)
app.use(function (req, res, next) {
  res.status(404).send("Sorry can't find that!")
});

var serverExpress = http.createServer(app);
serverExpress.listen(app.get('port'),()=>{
  console.log('HEALTH-ACTIVITY SERVER : '+ app.get('port'));
});