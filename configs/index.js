const pgpromise = require('bluebird');
const initOptions = {
    promiseLib: pgpromise,
};

const config = {
    host: 'localhost',
    port: 5432,
    database: 'rsb_b',
    user: 'postgres',
    password:'PostG_for_KIC'
};

const pgp = require('pg-promise')(initOptions);
const db = pgp(config);

const env     = process.env.NODE_ENV || 'development',
    winston     = require('winston'),
    tsFormat    = () => (new Date()).toLocaleTimeString(),
    logDir      = 'logs',
    logger      =  new (winston.Logger)({
        transports:[
            new (winston.transports.Console)({
                timestamp: tsFormat,
                colorize: true,
                level: 'info'
            }),
            new (require('winston-daily-rotate-file'))({
                filename: `${logDir}/-haci.log`,
                timestamp: tsFormat,
                datePattern: 'yyyy-MM-dd',
                prepend: true,
                level: env == 'development' ? 'verbose': 'info'
            })
        ]
    });

module.exports = {
    appSecret : 'HeartlandAlliance@2019!',
    logger: logger,
    logDirectory: logDir,
    db: db,
    getInfosFromFileName: function(filename){

        let t = filename.split('_');
        var _file       = t[0],
            _site     = t[1];

        // var _site = filename.substr(0,6),
        // _date     = filename.substr(7,8),
        // _version  = filename.substr(16,1);

        return {site: _site, file: _file};
    },
    getDate : function(){
    var date = new Date(),
        year = date.getFullYear(),
        month = (date.getMonth() + 1).toString(),
        formatedMonth = (month.length === 1) ? ("0" + month) : month,
        day = date.getDate().toString(),
        formatedDay = (day.length === 1) ? ("0" + day) : day,
        hour = date.getHours().toString(),
        formatedHour = (hour.length === 1) ? ("0" + hour) : hour,
        minute = date.getMinutes().toString(),
        formatedMinute = (minute.length === 1) ? ("0" + minute) : minute,
        second = date.getSeconds().toString(),
        formatedSecond = (second.length === 1) ? ("0" + second) : second;
        return year+formatedMonth+formatedDay;
    // return formatedDay + "-" + formatedMonth + "-" + year + " " + formatedHour + ':' + formatedMinute + ':' + formatedSecond;
}};